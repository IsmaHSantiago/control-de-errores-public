package main

import (
	"fmt"

	"./src/Controladores/EjemploControler"
	"./src/Controladores/HTTPErrorsControler"

	"./src/Modulos/Bugs"
	"./src/Modulos/Logs"
	"./src/Modulos/Variables"

	iris "gopkg.in/kataras/iris.v6"
	"gopkg.in/kataras/iris.v6/adaptors/httprouter"
	"gopkg.in/kataras/iris.v6/adaptors/view"
)

func main() {
	//############################################################
	defer func() {
		if err := MoLog.MyLogFile.Close(); err != nil {
			Bugs.SaveError(err)
			panic(err)
		}
	}()

	//########################  Init ####################################
	app := iris.New()
	app.Adapt(httprouter.New())

	//######################## ESTO DEBEN AGREGAR A SU RUTEADOR ##########
	app.Adapt(MoLog.MyFileLogger())
	//####################################################################

	app.Adapt(view.HTML("./Public/Vistas", ".html").Reload(true))

	app.Set(iris.OptionCharset("UTF-8"))
	app.StaticWeb("/icono", "./Public/Recursos/Generales/img")
	app.StaticWeb("/css", "./Public/Recursos/Generales/css")
	app.StaticWeb("/js", "./Public/Recursos/Generales/js")
	app.StaticWeb("/Plugins", "./Public/Recursos/Generales/Plugins")
	app.StaticWeb("/scripts", "./Public/Recursos/Generales/scripts")
	app.StaticWeb("/img", "./Public/Recursos/Generales/img")
	app.StaticWeb("/Comprobantes", "./Public/Recursos/Locales/comprobantes")
	app.StaticWeb("/Locales", "./Public/Recursos/Locales")

	var DataCfg = MoVar.CargaSeccionCFG(MoVar.SecDefault)

	//Prueba

	//Error 403
	app.OnError(iris.StatusForbidden, HTTPErrors.Error403)
	//Error 404
	app.OnError(iris.StatusNotFound, HTTPErrors.Error404)
	//Error 500
	app.OnError(iris.StatusInternalServerError, HTTPErrors.Error500)

	app.Get("/Prueba1/:Val", EjemploControler.HazAlgoEnElControlador)
	app.Get("/Prueba2/:Val", EjemploControler.VistaEjemplo)

	if DataCfg.Puerto != "" {
		fmt.Println("Ejecutandose en el puerto: ", DataCfg.Puerto)
		fmt.Println("Acceder a la siguiente url: ", DataCfg.BaseURL)
		app.Listen(":" + DataCfg.Puerto)
	} else {
		fmt.Println("Ejecutandose en el puerto: 8080")
		fmt.Println("Acceder a la siguiente url: localhost")
		app.Listen(":8080")
	}

}
