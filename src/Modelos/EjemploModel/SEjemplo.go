package EjemploModel

import (
	"html/template"

	"gopkg.in/mgo.v2/bson"
)

//ECampo1Ejemplo Estructura de campo de Ejemplo
type ECampo1Ejemplo struct {
	Campo1   string
	IEstatus bool
	IMsj     string
	Ihtml    template.HTML
}

//ECampo2Ejemplo Estructura de campo de Ejemplo
type ECampo2Ejemplo struct {
	Campo2   string
	IEstatus bool
	IMsj     string
	Ihtml    template.HTML
}

//Ejemplo estructura de Ejemplo mongo
type Ejemplo struct {
	ID bson.ObjectId
	ECampo1Ejemplo
	ECampo2Ejemplo
}

//SSesion estructura de variables de sesion de Usuarios del sistema
type SSesion struct {
	Name          string
	MenuPrincipal template.HTML
	MenuUsr       template.HTML
}

//SIndex estructura de variables de index
type SIndex struct {
	SResultados        bool
	SRMsj              string
	STituloTabla       string
	SUrlDeDatos        string
	SNombresDeColumnas []string
	SModeloDeColumnas  []map[string]interface{}
	SRenglones         map[string]interface{}
}

//SReportes estructura para los reportes del sistema
type SReportes struct {
	Tipos  template.HTML
	Modulo string
}

//SEjemplo estructura de Ejemplo para la vista
type SEjemplo struct {
	SEstado bool
	SMsj    string
	Ejemplo
	SIndex
	SSesion
	SReportes
}
