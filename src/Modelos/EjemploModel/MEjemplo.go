package EjemploModel

import "errors"

//HazAlgoEnElModelo función ejemplo de retorno de errores en el Modelo
func HazAlgoEnElModelo(ArgumentoDeEntrada bool) (string, error) {
	if !ArgumentoDeEntrada {
		return "", errors.New("el RFC no es válido")
	}
	return "Se recibió un argumento adecuado", nil
}

func FuncionSinError() string {
	return "Hola"
}
