package EjemploControler

import (
	"html/template"
	"strconv"

	"../../Modelos/EjemploModel"

	"../../Modulos/Bugs"
	"../../Modulos/Variables"

	iris "gopkg.in/kataras/iris.v6"
)

//##########< Variables Generales > ############

var moduloLocal = MoVar.ModuloLocal

//HazAlgoEnElControlador función de ejemplo en el Controlador
func HazAlgoEnElControlador(ctx *iris.Context) {

	Parametro := ctx.Param("Val")

	ArgumentoDeParametro, err := strconv.ParseBool(Parametro)
	if err != nil {
		Bugs.SaveError(err, ctx)
		ctx.Write([]byte("Ocurrió el siguiente error al parsear a boolean el parámetro: " + Parametro + " debido a: " + err.Error()))
		return
	}

	respuesta, err := EjemploModel.HazAlgoEnElModelo(ArgumentoDeParametro)
	if err != nil {
		Bugs.SaveError(err, ctx)
		ctx.Write([]byte("Ocurrió el siguiente error al Evaluar tu parámetro : " + Parametro + " en la función del modelo debido a: " + err.Error()))
		return
	}

	if respuesta != "" {
		ctx.Write([]byte("Se recibió la siguiente respuesta: " + respuesta))
	}

}

//VistaEjemplo funcion para mostrar ejemplo de errores en vista y reportear problemas
func VistaEjemplo(ctx *iris.Context) {
	var Send EjemploModel.SEjemplo
	var EstatusPeticion bool
	var errores string

	//#########################################################
	tipos, err := Bugs.CargaComboTiposDeProblemas()
	if err != nil {
		Bugs.SaveError(err, ctx)
	}
	Send.SReportes.Tipos = template.HTML(tipos)
	Send.SReportes.Modulo = moduloLocal
	//#########################################################

	Parametro := ctx.Param("Val")

	ArgumentoDeParametro, err := strconv.ParseBool(Parametro)
	if err != nil {
		errores += " || " + err.Error()
		EstatusPeticion = true
		Bugs.SaveError(err, ctx)
	}

	respuesta, err := EjemploModel.HazAlgoEnElModelo(ArgumentoDeParametro)
	if err != nil {
		errores += " || " + err.Error()
		EstatusPeticion = true
		Bugs.SaveError(err, ctx)
	}

	_ := EjemploModel.FuncionSinError()

	if EstatusPeticion {
		Send.SEstado = false
		Send.SMsj = "Existen los siguientes Errores : " + errores
	} else {
		Send.SEstado = true
		Send.SMsj = respuesta
	}

	ctx.Render("Ejemplo/Ejemplo.html", Send)
}
