package HTTPErrors

import (
	"errors"
	"html/template"

	"../../Modelos/HTTPErrorsModel"
	"../../Modulos/Bugs"

	"gopkg.in/kataras/iris.v6"
)

//Error403 es una función que maneja el error 403
func Error403(ctx *iris.Context) {

	Bugs.SaveError(errors.New("ERROR 403 ACCESO DENEGADO"), ctx)

	var Error HTTPErrorsModel.HTTPError
	Error.Mensaje = template.HTML(` No tienes acceso a esta pantalla y/o esta acción.
		Te sugerimos que regreses a <a href="/">CONTROL DE ERRORES Korê</a>.`)

	Error.Tipo = template.HTML("ERROR 403 ACCESO DENEGADO")
	Error.Soporte = template.HTML(`Si crees que este mensaje no debería ser un error, contactanos <a href="#">ayuda@korê.com</a>`)

	ctx.Render("Base/AAHTTPErrors.html", Error)
}

//Error404 es una función que maneja el error 404
func Error404(ctx *iris.Context) {
	Bugs.SaveError(errors.New("ERROR 404 RECURSO NO ENCONTRADO"), ctx)
	var Error HTTPErrorsModel.HTTPError
	Error.Mensaje = template.HTML(` No se encontró la URL que solicitaste. Por Favor, asegúrate de usar el link correcto.
		Te sugerimos que regreses a <a href="/">CONTROL DE ERRORES Korê</a>.`)

	Error.Tipo = template.HTML("ERROR 404 RECURSO NO ENCONTRADO")
	Error.Soporte = template.HTML(`Si crees que este mensaje no debería ser un error, contactanos <a href="#">ayuda@korê.com</a>`)

	ctx.Render("Base/AAHTTPErrors.html", Error)
}

//Error500 es una función que maneja el error 500
func Error500(ctx *iris.Context) {
	Bugs.SaveError(errors.New("ERROR 500 ERROR INTERNO"), ctx)
	var Error HTTPErrorsModel.HTTPError
	Error.Mensaje = template.HTML(` Esto nos da mucha pena, no debió ocurrir, pero trabajaremos en ello.
        							Te sugerimos que regreses a <a href="/">CONTROL DE ERRORES Korê</a>.`)
	Error.Tipo = template.HTML("ERROR 500 ERROR INTERNO")
	Error.Soporte = template.HTML(`Por favor comunícate con nosotros para reportar el problema a <a href="#">ayuda@korê.com</a>`)

	ctx.Render("Base/AAHTTPErrors.html", Error)
}
