package MoLog

import (
	"log"
	"os"

	iris "gopkg.in/kataras/iris.v6"
)

//MyLogFile apuntador para el archivo Log
var MyLogFile *os.File

//LogInit inicia creando el archivo log
func LogInit() *os.File {

	root, err := os.Getwd()
	if err != nil {
		panic(err)
	}

	existe, err := Exists(root + `\ErrorLog.txt`)
	if err != nil {
		panic(err)
	}

	if !existe {
		f, err := os.Create("ErrorLog.txt")
		if err != nil {
			panic(err)
		}
		return f
	}

	file, err := os.OpenFile(root+`\ErrorLog.txt`, os.O_APPEND, 0666)
	if err != nil {
		panic(err)
	}
	return file

}

//MyFileLogger regresa una función como política de registro de Iris
func MyFileLogger() iris.LoggerPolicy {
	MyLogFile = LogInit()
	myLogger := log.New(MyLogFile, "", log.LstdFlags)

	return func(mode iris.LogMode, message string) {
		if mode == iris.ProdMode {
			myLogger.Println(message)
		}
	}
}

//Exists checa si un path o archivo existe
func Exists(name string) (bool, error) {
	_, err := os.Stat(name)
	if err == nil {
		return true, nil
	}
	if os.IsNotExist(err) {
		return false, nil
	}

	return true, err
}
