package MoGeneral

import (
	"encoding/json"
	"reflect"
	"time"

	"../../Modulos/Crypto"
	"../../Modulos/Estructuras"
	"../../Modulos/Variables"
)

var dataM = MoVar.CargaSeccionCFG(MoVar.SecMongo)

//########### GENERALES #######################################

//EstaVacio verifica si un objeto está vacío o no
func EstaVacio(object interface{}) bool {
	if object == nil {
		return true
	} else if object == "" {
		return true
	} else if object == false {
		return true
	}

	if reflect.ValueOf(object).Kind() == reflect.Struct {
		empty := reflect.New(reflect.TypeOf(object)).Elem().Interface()
		if reflect.DeepEqual(object, empty) {
			return true
		}
	}
	return false
}

//GetDocAsk regresa una estructura ya con cabecera
func GetDocAsk(Metodo string) MoEstructuras.DocAsk {
	var Doc MoEstructuras.DocAsk
	Doc.Header.From = MoVar.ModuloLocal
	Doc.Header.Process = Metodo
	Doc.Header.Date = time.Now()
	return Doc
}

//EncriptaStructToSend hace todo
func EncriptaStructToSend(Metodo string, Estruct interface{}) (MoEstructuras.Send, error) {
	var Send MoEstructuras.Send
	var err error

	Doc := GetDocAsk(Metodo)

	Doc.Body.Content, err = json.Marshal(Estruct)
	if err != nil {
		return Send, err
	}

	Send, err = Crypto.CodificaSend(Doc)
	if err != nil {
		return Send, err
	}

	return Send, nil

}
