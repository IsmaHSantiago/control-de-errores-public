package Bugs

import (
	"encoding/json"
	"errors"
	"net/url"
	"runtime"
	"time"

	"../../Modulos/Crypto"
	"../../Modulos/Estructuras"
	"../../Modulos/General"
	"../../Modulos/Variables"
	"../../Modulos/WebService"

	iris "gopkg.in/kataras/iris.v6"
	"gopkg.in/mgo.v2/bson"
)

var dataWs = MoVar.CargaSeccionCFG(MoVar.SecWsMonitoreo)

var urlInsertBug = dataWs.BaseURL + "/Bug/alta"
var urlGetTipos = dataWs.BaseURL + "/Bug/GetTipos"

var tiposGlobal []Tipos

//AllDataError es una estructura que contiene todos los datos a guardar del Error
type AllDataError struct {
	ID       bson.ObjectId `bson:"_id,omitempty"`
	Request  DataReq       `bson:"Request"`
	Usuario  string        `bson:"Usuario"`
	Function string        `bson:"Function"`
	Error    string        `bson:"Error"`
	Estatus  bson.ObjectId `bson:"Estatus,omitempty"`
	Date     time.Time     `bson:"Date"`
}

//DataReq es una estructura que contiene todos los datos a guardar de la petición para el Error
type DataReq struct {
	Method     string              `bson:"Method"`
	Path       string              `bson:"Path"`
	Proto      string              `bson:"Proto"`
	Explorador string              `bson:"Explorador"`
	Lenguaje   string              `bson:"Lenguaje"`
	OS         string              `bson:"OS"`
	Host       string              `bson:"Host"`
	URLParams  map[string]string   `bson:"URLParams"`
	Form       map[string][]string `bson:"Form"`
	RemoteAddr string              `bson:"RemoteAddr"`
}

//Tipos Estructura para recibir tipos de reportes
type Tipos struct {
	ID    bson.ObjectId `bson:"_id,omitempty"`
	Valor string        `bson:"Valor"`
}

//SaveError guarda errores con datos de sesión
func SaveError(Error error, ctx ...*iris.Context) {

	var ErrorToSave AllDataError
	var ReqData DataReq

	ErrorToSave.Date = time.Now()

	ErrorToSave.Usuario = "User"
	ErrorToSave.Error = Error.Error()

	pc, _, _, ok := runtime.Caller(1)
	details := runtime.FuncForPC(pc)
	if ok && details != nil {
		ErrorToSave.Function = details.Name()
	}

	if len(ctx) > 0 {
		ReqData.Method = ctx[0].Method()
		ReqData.Path = ctx[0].Path()
		ReqData.Proto = ctx[0].Request.Proto
		ReqData.Explorador = ctx[0].Request.Header.Get("User-Agent")
		ReqData.Lenguaje = ctx[0].Request.Header.Get("Accept-Language")
		ReqData.OS = runtime.GOOS
		ReqData.Host = ctx[0].ServerHost()
		ReqData.URLParams = ctx[0].URLParams()
		ReqData.Form = ctx[0].FormValues()
		ReqData.RemoteAddr = ctx[0].RemoteAddr()

		ctx[0].Framework().Log(iris.ProdMode, `|#| Ocurrió Error: `+Error.Error()+` |#| En la funcion: `+ErrorToSave.Function+` |#| Por una petición tipo: `+ReqData.Method+` |#| A la URL: `+ReqData.Path)
	}

	ErrorToSave.Request = ReqData

	ToSend, err := MoGeneral.EncriptaStructToSend("Servicio_Monitor_AltaBug", ErrorToSave)
	if err == nil {
		Send := url.Values{}
		Send.Add("X1X2X3", string(ToSend.X1X2X3))
		Send.Add("Y1Y2Y3", string(ToSend.Y1Y2Y3))
		WebService.ConsultaWSPostSinRespuesta(urlInsertBug, Send, time.Duration(60))
	} else {
		ctx[0].Framework().Log(iris.ProdMode, `|#| Ocurrió Error: `+err.Error()+` |#| En la funcion: Bugs.SaveError() |#| Al intentar enviar al WS de Monitoreo el error: `+Error.Error()+` |#| En la funcion: `+ErrorToSave.Function+` |#| Por una petición tipo: `+ReqData.Method+` |#| A la URL: `+ReqData.Path)
	}
}

//CargaComboTiposDeProblemas Carga el combo para un multi select
func CargaComboTiposDeProblemas() (string, error) {
	templ := ``
	templ += `<option value="">--SELECCIONE--</option>`

	if tiposGlobal == nil || len(tiposGlobal) == 0 {
		Send, err := WebService.ConsultaWSGet(urlGetTipos, time.Duration(30))
		if err != nil {
			templ += `<option value="OTRO">OTRO</option>`
			return templ, err
		}

		DataBytes, err := Crypto.DecodificaSend(Send)
		if err != nil {
			SaveError(err)
			templ += `<option value="OTRO">OTRO</option>`
			return templ, err
		}

		var Document MoEstructuras.DocAnswer
		err = json.Unmarshal(DataBytes, &Document)
		if err != nil {
			templ += `<option value="OTRO">OTRO</option>`
			return templ, err
		}

		if !MoGeneral.EstaVacio(Document) {
			if Document.Header.Status {

				err = json.Unmarshal(Document.Body.Content, &tiposGlobal)
				if err != nil {
					templ += `<option value="OTRO">OTRO</option>`
					return templ, err
				}

				for _, v := range tiposGlobal {
					templ += `<option value="` + v.Valor + `">` + v.Valor + `</option>`
				}

			} else {
				templ += `<option value="OTRO">OTRO</option>`
				return templ, errors.New(Document.Header.Msg)
			}
		}

		templ += `<option value="OTRO">OTRO</option>`

		return templ, nil
	}

	for _, v := range tiposGlobal {
		templ += `<option value="` + v.Valor + `">` + v.Valor + `</option>`
	}

	templ += `<option value="OTRO">OTRO</option>`
	return templ, nil
}
