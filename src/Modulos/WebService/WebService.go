package WebService

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/url"
	"strings"
	"time"

	"../../Modulos/Estructuras"
)

//ConsultaWSGet consulta un web service con un método Get, (la URL debe tener ya concatenadas las variables y sus valores.)
//responde con un arreglo de bytes listo para convertise a un tipo de documento de comunicación.
func ConsultaWSGet(URL string, Timeout time.Duration) (MoEstructuras.Send, error) {
	var Send MoEstructuras.Send

	var netClient = &http.Client{
		Timeout: time.Second * Timeout,
	}

	response, err := netClient.Get(URL)
	if err != nil {
		return Send, err
	}
	defer response.Body.Close()

	responseData, err := ioutil.ReadAll(response.Body)
	if err != nil {
		return Send, err
	}

	err = json.Unmarshal(responseData, &Send)
	if err != nil {
		return Send, err
	}

	return Send, nil
}

//ConsultaWSPost consulta un web service con un método Post, se deben añadir los valores del formulario.
//responde con un arreglo de bytes listo para convertise a un tipo de documento de comunicación.
func ConsultaWSPost(URL string, Data url.Values, Timeout time.Duration) (MoEstructuras.Send, error) {
	var Send MoEstructuras.Send

	var netClient = &http.Client{
		Timeout: time.Second * Timeout,
	}

	response, err := netClient.Post(URL, "application/x-www-form-urlencoded", strings.NewReader(Data.Encode()))
	if err != nil {
		return Send, err
	}
	defer response.Body.Close()

	fmt.Println(json.NewDecoder(response.Body).Decode(&Send))

	responseData, err := ioutil.ReadAll(response.Body)
	if err != nil {
		return Send, err
	}

	err = json.Unmarshal(responseData, &Send)
	if err != nil {
		return Send, err
	}

	return Send, nil
}

//ConsultaWSPostSinRespuesta consulta un web service con un método Post, se deben añadir los valores del formulario.
//responde con un arreglo de bytes listo para convertise a un tipo de documento de comunicación.
func ConsultaWSPostSinRespuesta(URL string, Data url.Values, Timeout time.Duration) (MoEstructuras.Send, error) {
	var Send MoEstructuras.Send

	var netClient = &http.Client{
		Timeout: time.Second * Timeout,
	}
	netClient.Post(URL, "application/x-www-form-urlencoded", strings.NewReader(Data.Encode()))
	return Send, nil
}

//ConsultaWSMethod consulta un web service especificando método, se deben añadir los valores del formulario.
//responde con un arreglo de bytes listo para convertise a un tipo de documento de comunicación.
func ConsultaWSMethod(Method, URL string, Data url.Values, Timeout time.Duration) (MoEstructuras.Send, error) {
	var Send MoEstructuras.Send

	var netClient = &http.Client{
		Timeout: time.Second * Timeout,
	}

	req, err := http.NewRequest(Method, URL, strings.NewReader(Data.Encode()))
	if err != nil {
		return Send, err
	}

	response, err := netClient.Do(req)
	if err != nil {
		return Send, err
	}
	defer response.Body.Close()

	responseData, err := ioutil.ReadAll(response.Body)
	if err != nil {
		return Send, err
	}

	err = json.Unmarshal(responseData, &Send)
	if err != nil {
		return Send, err
	}

	return Send, nil
}

//ReadRequestToSend lee la petición, obtiene los parámetros y regresa una variable Send.
func ReadRequestToSend(Request *http.Request) (MoEstructuras.Send, error) {
	var Send MoEstructuras.Send
	er := Request.ParseForm()
	if er != nil {
		return Send, er
	}

	Send.X1X2X3 = []byte(Request.FormValue("X1X2X3"))
	Send.Y1Y2Y3 = []byte(Request.FormValue("Y1Y2Y3"))

	return Send, nil
}
