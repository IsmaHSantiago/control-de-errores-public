$( document ).ready(function() {
    comprobarPagina("Modulo_ejemplo");
    $("#LugarProblema").change(function(){
        if ($("#LugarProblema").val()=="OTRO"){
            $("#Especificacion").show();
        }else{
            $("#Especificacion").hide();
        }
    });
});
    
function ReportarProblema(modulo){
    var estatus = false;
    var errores = [];
    
    var tipo = $("#LugarProblema").val();
    if (tipo == ""){
        estatus = true;
        errores.push("Debe Seleccionar un tipo de problema.");
    }

    var especificacion = $("#EspecificacionProblema").val().trim();
    if (tipo == "OTRO" && especificacion == ""){
        estatus = true;
        errores.push("Debe Especificar qué OTRO tipo de problema que ocurrió.");
    }

    var contenido = $("#ContenidoProblema").val().trim();
    if (contenido == ""){
        estatus = true;
        errores.push("Por favor cuentanos qué pasó y cómo podemos reproducir el problema que ocurrió.");
    }


    if (!estatus){
        var Datos = CargaDatos();
        $.ajax({
            url: "http://192.168.1.55:8085/Bug/altareporte",
            type: 'POST',
            dataType: 'json',
            data: {
				Especificacion: especificacion,
                Contenido: contenido,
                Tipo: tipo,
                Modulo: modulo,
                Path: Datos.Path,
                Url: Datos.Url,
                Proto: Datos.Proto,
                Lenguaje: Datos.LN,
                Os: Datos.OS,
                Explorador: Datos.Browser,
                Cookie: navigator.cookieEnabled,
                InputName: Datos.Input
            },
            success: function(data) {
                if (data != null) {
                        $("#LugarProblema").val("")
                        $("#EspecificacionProblema").val("");
                        $("#ContenidoProblema").val("");
                        alertify.message("Reporte enviado.");                        
                } else {
                        alertify.error("Hubo un problema al emitir su reporte, por favor vuelva a intentar.");
                }
                 $("#CerrarModalProblemas").click();
            },
            error: function(a, b, c) {
                alertify.error("Hubo un problema al recibir información del servidor, verifique su conexión.");                
                $("#CerrarModalProblemas").click();
            },
        });
    }else{
        errores.forEach(function(item, index){
            alertify.error(item);
        });        
    }
        
}


function comprobarPagina(modulo){
        var data = $('#objeto').val();
        if (data == undefined){
            alertify.error("Al parecer tu Pantalla no se cargó completa.");
            alertify.error("Intentaremos notificar con tu proveedor de Servicio.");

            var Datos = CargaDatos();

                 $.ajax({
                    url: "http://192.168.1.55:8085/Bug/altavista",
                    type: 'POST',
                    dataType: 'json',
                    data: {
                        Modulo: modulo,
                        Path: Datos.Path,
                        Url: Datos.Url,
                        Proto: Datos.Proto,
                        Lenguaje: Datos.LN,
                        Os: Datos.OS,
                        Explorador: Datos.Browser,
                        Cookie: navigator.cookieEnabled,
                        InputName: Datos.Input
                    },
                    success: function(data) {
                        if (data != null) {
                            alertify.message("Se ha emitido un reporte de lo sucedido")
                            alertify.message("Su Proveedor de servicios se comunicará con usted a la brevedad.");
                        } else {
                            alertify.error("No pudo emitirse el reporte, los sentimos, por favor comuníquese con su proveedor de servicios para solucionar lo más pronto posible este problema.");
                        }
                    }
                });
        }
    }

    function CargaDatos(){        
        var pathname = window.location.pathname;
        var url = window.location.href; 
        var protocol = window.location.protocol
        var userLang = navigator.language || navigator.userLanguage; 

        var userAgent = navigator.userAgent || navigator.vendor || window.opera;

         var browserName;

        if ((window.opr && opr.addons) || window.opera || (navigator.userAgent.indexOf(' OPR/') >= 0))
        browserName = "Opera";                
        if (typeof InstallTrigger !== 'undefined')
        browserName = "Firefox";                
        if (Object.prototype.toString.call(window.HTMLElement).indexOf('Constructor') > 0)
        browserName = "Safari";                 
        if ((/*@cc_on!@*/false) || (document.documentMode))
        browserName = "IE";                 
        if (!(document.documentMode) && window.StyleMedia)
        browserName = "Edge";
        if (window.chrome && window.chrome.webstore)
        browserName = "Chrome";

        var dataultima;
        var y = document.body.querySelectorAll("input");
        
        dataultima = y[y.length-1];

        return {Path:pathname, Url: url, Proto: protocol, LN: userLang, OS:userAgent, Browser: browserName, Input: dataultima.id}

    }